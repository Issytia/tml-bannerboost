using Microsoft.Xna.Framework;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using Terraria.ModLoader.Config;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace IssyBannerBoost {
	using BE = ItemID.BannerEffect;

	using IBB = IssyBannerBoost;

	public class IssyBannerBoost : Mod {
		internal static IssyBannerBoost Instance { get; private set; }
		public override void Load() { Instance = this; }
		public override void Unload() {
			Instance = null;
			levels = null;
			IssyNPC.debug_seen = null;
		}

		internal static int MaxLevel { get { return IssyConfig.Instance.MaxLevel; } }
		static float LevelStrength { get { return IssyConfig.Instance.Strength; } }
		internal static bool KnockbackImmunity { get { return IssyConfig.Instance.KnockbackImmunity; } }
		static bool ExcludeArmy { get { return IssyConfig.Instance.Exclude; } }
		internal static int Debug { get { return IssyConfig.Instance.Debug; } }

		static Dictionary<int, int> levels = new Dictionary<int, int>();
		internal static int Level(int id) {
			if (Debug == -1 || !ValidID(id))
				return 0;
			if (Debug == 1)
				return MaxLevel;
			if (!levels.ContainsKey(id))
				levels[id] = Math.Min((int) ((float) NPC.killCount[id] / PerLevel(id)), MaxLevel);
			return levels[id];
		}
		internal static bool CheckLevel(int id) {
			if (ValidID(id) && Level(id) < MaxLevel) {
				int new_level = Math.Min((int) ((float) NPC.killCount[id] / PerLevel(id)), MaxLevel);
				if (new_level != 0 && levels[id] < new_level) {
					levels[id] = new_level;
					return true;
				}
			}

			return false;
		}
		static float PerLevel(int id) {
			float ret = IssyConfig.Instance.KillsPerLevel;
			if (!ValidID(id))
				return ret;

			return ret * KillsFactor(id);
		}

		internal static bool ValidID(int id) {
			if (id <= 0 || (ExcludeArmy && IsArmy(id)))
				return false;

			return true;
		}
		internal static bool IsArmy(int id) => Item.BannerToNPC(id) switch {
			NPCID.DD2Betsy or NPCID.DD2DarkMageT1 or NPCID.DD2DarkMageT3 or NPCID.DD2DrakinT2 or
			NPCID.DD2DrakinT3 or NPCID.DD2GoblinBomberT1 or NPCID.DD2GoblinBomberT2 or
			NPCID.DD2GoblinBomberT3 or NPCID.DD2GoblinT1 or NPCID.DD2GoblinT2 or NPCID.DD2GoblinT3 or
			NPCID.DD2JavelinstT1 or NPCID.DD2JavelinstT2 or NPCID.DD2JavelinstT3 or NPCID.DD2KoboldFlyerT2 or
			NPCID.DD2KoboldFlyerT3 or NPCID.DD2KoboldWalkerT2 or NPCID.DD2KoboldWalkerT3 or
			NPCID.DD2LightningBugT3 or NPCID.DD2OgreT2 or NPCID.DD2OgreT3 or NPCID.DD2SkeletonT1 or
			NPCID.DD2SkeletonT3 or NPCID.DD2WitherBeastT2 or NPCID.DD2WitherBeastT3 or NPCID.DD2WyvernT1 or
			NPCID.DD2WyvernT2 or NPCID.DD2WyvernT3 => true,
			_ => false
		};

		internal static float DamageMult(int id) {
			if (!ValidID(id))
				return 1.0f;

			BE eff = Effect(id);
			float ret = Main.expertMode switch {
				true => eff.ExpertDamageDealt,
				false => eff.NormalDamageDealt
			};

			return 1 + ((float) Level(id) / MaxLevel * LevelStrength * (ret - 1));
		}
		internal static float DefenseMult(int id) {
			if (!ValidID(id))
				return 1.0f;

			BE eff = Effect(id);
			float ret = Main.expertMode switch {
				true => eff.ExpertDamageReceived,
				false => eff.NormalDamageReceived
			};

			return 1 - ((float) Level(id) / MaxLevel * LevelStrength * (1 - ret));
		}

		private static BE Effect(int id) {
			return ItemID.Sets.BannerStrength[Item.BannerToItem(id)];
		}
		private static float KillsFactor(int id) {
			return (float) ItemID.Sets.KillsToBanner[Item.BannerToItem(id)] / ItemID.Sets.DefaultKillsForBannerNeeded;
		}
	}

	public class IssyNPC : GlobalNPC {
		public override bool InstancePerEntity { get { return true; } }

		internal static HashSet<string> debug_seen = new HashSet<string>();

		int m_banner = -2;

		bool HasBanner(NPC npc) {
			if (m_banner == -2)
				m_banner = Item.NPCtoBanner(npc.BannerID());
			if (!IBB.ValidID(m_banner) || npc.townNPC || npc.CountsAsACritter) {
				if (IBB.Debug != 0 && !debug_seen.Contains(npc.TypeName)) {
					debug_seen.Add(npc.TypeName);
					if (m_banner == 0)
						Main.NewText(String.Format("No banner for {0}", npc.TypeName));
					else
						Main.NewText(String.Format("Excluding {0} ({1})", npc.TypeName, m_banner));
				}
				m_banner = -1;
				return false;
			}
			return true;
		}

		public override void OnKill(NPC npc) {
			if (HasBanner(npc) && IBB.CheckLevel(m_banner))
				Main.NewText(String.Format("You have reached level {0} against {1}!", IBB.Level(m_banner), npc.TypeName), new Color(48, 255, 76));
		}

		public override void ModifyHitNPC(NPC npc, NPC target, ref NPC.HitModifiers mod) {
			if (target.townNPC && HasBanner(npc)) {
				float m = IBB.DefenseMult(m_banner);
				mod.TargetDamageMultiplier *= m;

				if (IBB.KnockbackImmunity && IBB.Level(m_banner) == IBB.MaxLevel)
					mod.Knockback *= 0;

				if (IBB.Debug != 0)
					Main.NewText(String.Format("NPC: {0}, {1} ({2})", m, mod.TargetDamageMultiplier.Value, m_banner));
			}
		}

		public override void ModifyHitPlayer(NPC npc, Player target, ref Player.HurtModifiers mod) {
			if (HasBanner(npc)) {
				float m = IBB.DefenseMult(m_banner);
				mod.IncomingDamageMultiplier *= m;

				if (IBB.KnockbackImmunity && IBB.Level(m_banner) == IBB.MaxLevel)
					mod.Knockback *= 0;

				if (IBB.Debug != 0)
					Main.NewText(String.Format("Player: {0}, {1} ({2})", m, mod.IncomingDamageMultiplier.Value, m_banner));
			}
		}

		public override void ModifyIncomingHit(NPC npc, ref NPC.HitModifiers mod) {
			if (HasBanner(npc)) {
				float m = IBB.DamageMult(m_banner);
				mod.TargetDamageMultiplier *= m;

				if (IBB.Debug != 0)
					Main.NewText(String.Format("Incoming: {0}, {1} ({2})", m, mod.TargetDamageMultiplier.Value, m_banner));
			}
		}
	}

	public class IssyProjectile : GlobalProjectile {
		public override void ModifyHitNPC(Projectile proj, NPC target, ref NPC.HitModifiers mod) {
			int id = proj.bannerIdToRespondTo;
			if (target.townNPC && IBB.ValidID(id)) {
				float m = IBB.DefenseMult(id);
				mod.TargetDamageMultiplier *= m;

				if (IBB.KnockbackImmunity && IBB.Level(id) == IBB.MaxLevel)
					mod.Knockback *= 0;

				if (IBB.Debug != 0)
					Main.NewText(String.Format("Projectile NPC: {0}, {1} ({2})", m, mod.TargetDamageMultiplier.Value, id));
			}
		}

		public override void ModifyHitPlayer(Projectile proj, Player target, ref Player.HurtModifiers mod) {
			int id = proj.bannerIdToRespondTo;
			if (IBB.ValidID(id)) {
				float m = IBB.DefenseMult(id);
				mod.IncomingDamageMultiplier *= m;

				if (IBB.KnockbackImmunity && IBB.Level(id) == IBB.MaxLevel)
					mod.Knockback *= 0;

				if (IBB.Debug != 0)
					Main.NewText(String.Format("Projectile Player: {0}, {1} ({2})", m, mod.IncomingDamageMultiplier.Value, id));
			}
		}
	}

	public class IssyBuff : GlobalBuff {
		static bool DisableBuff { get { return IssyConfig.Instance.DisableBuff; } }

		public override void Update(int type, Player player, ref int index) {
			if (DisableBuff && type == BuffID.MonsterBanner) {
				player.DelBuff(index--);
			}
		}
	}

	[Label("Server config")]
	class IssyConfig : ModConfig {
		public override ConfigScope Mode => ConfigScope.ServerSide;
		public static IssyConfig Instance => ModContent.GetInstance<IssyConfig>();

		[Label("Kills per level")]
		[Increment(5)]
		[Range(5, 100)]
		[DefaultValue(20)]
		[Slider]
		public int KillsPerLevel;

		[Label("Max level")]
		[Increment(1)]
		[Range(1, 200)]
		[DefaultValue(10)]
		[Slider]
		public int MaxLevel;

		[Label("Strength at max level relative to banner")]
		[Increment(0.1f)]
		[Range(0.1f, 5f)]
		[DefaultValue(1f)]
		[Slider]
		public float Strength;

		[Label("Knockback immunity at max level")]
		[DefaultValue(false)]
		public bool KnockbackImmunity;

		[Label("Exclude Old One's Army")]
		[DefaultValue(false)]
		public bool Exclude;

		[Label("Disable placed banner buff")]
		[DefaultValue(true)]
		public bool DisableBuff;

		[Label("Debug")]
		[DefaultValue(0)]
		[Range(-1, 2)]
		[Slider]
		public int Debug;
	}
}
